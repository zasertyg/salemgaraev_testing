import org.junit.Assert;
import org.testng.annotations.Test;

public class LoginTests extends TestBase {
    @Test
    public void loginWithValidCreditionals() throws Exception {
        Account user = new Account("tes.testing2017@yandex.ru", "testing");
        app.loginHelper.logout();
        app.loginHelper.login(user);
        Assert.assertEquals(app.loginHelper.isLoggedIn(user), true);
    }
}
