import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.*;

public class HelperBase {
    protected WebDriver driver;
    protected AppManager manager;

    public HelperBase(AppManager manager) {
        this.manager = manager;
        driver = manager.driver;
    }

    public void fillTheField(String field, String value) {
        WebElement element = driver.findElement(By.name(field));
        driver.findElement(By.name(field)).clear();
        driver.findElement(By.name(field)).sendKeys(value);
    }

    public void fillTheSelect(String field, String value) {
        new Select(driver.findElement(By.name(field))).selectByVisibleText(value);
    }

}
