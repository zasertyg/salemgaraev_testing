import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AppManager {
    private String baseUrl;
    protected WebDriver driver;
    protected LoginHelper loginHelper;
    protected NavigationHelper navigationHelper;
    protected ContactHelper contactHelper;
    protected GroupHelper groupHelper;
    protected StringBuffer verificationErrors = new StringBuffer();
    protected boolean acceptNextAlert = true;

    private static ThreadLocal <AppManager> app = new ThreadLocal <AppManager>();

    public AppManager() {
        driver = new FirefoxDriver();
        baseUrl = "https://www.postable.com/";
        loginHelper = new LoginHelper(this);
        navigationHelper = new NavigationHelper(this, baseUrl);
        contactHelper = new ContactHelper(this);
        groupHelper = new GroupHelper(this);
    }


    @AfterClass(alwaysRun = true)
    protected void stop() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public static AppManager getInstance (){
        if (app == null) {
            AppManager newInstance = new AppManager();
            newInstance.navigationHelper.goToHomePage();
            app.set(newInstance);
        }
        return app.get();
    }


    protected boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    protected String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
