import org.testng.annotations.BeforeClass;

public class AuthBase extends TestBase {
    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        app.loginHelper.login(new Account("tes.testing2017@yandex.ru", "testing"));
    }
}
