import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class MyTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "https://www.postable.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    @Test
    public void testMyTest() throws Exception {
        driver.get(baseUrl + "/address-book/");
        driver.findElement(By.linkText("LOGIN")).click();
        driver.findElement(By.id("InputEmail1")).clear();
        driver.findElement(By.id("InputEmail1")).sendKeys("tes.testing2017@yandex.ru");
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys("testing");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.linkText("ADDRESS BOOK")).click();
        driver.findElement(By.linkText("ADD NEW")).click();
        driver.findElement(By.name("contact[firstname]")).clear();
        driver.findElement(By.name("contact[firstname]")).sendKeys("Sam");
        driver.findElement(By.name("contact[lastname]")).clear();
        driver.findElement(By.name("contact[lastname]")).sendKeys("Smith");
        driver.findElement(By.id("to-address")).clear();
        driver.findElement(By.id("to-address")).sendKeys("Baum");
        driver.findElement(By.xpath("//body[@id='contacts']/div[6]/div[2]/span[3]")).click();
        driver.findElement(By.name("contact[phone]")).clear();
        driver.findElement(By.name("contact[phone]")).sendKeys("(789) 347-9322");
        new Select(driver.findElement(By.name("contact[birth_month]"))).selectByVisibleText("Apr");
        new Select(driver.findElement(By.name("contact[birth_day]"))).selectByVisibleText("7th");
        driver.findElement(By.cssSelector("button.button-panel.button-save-contact")).click();
        driver.findElement(By.linkText("new group")).click();
        driver.findElement(By.name("group_name")).clear();
        driver.findElement(By.name("group_name")).sendKeys("Work");
        driver.findElement(By.linkText("Create group")).click();
        driver.findElement(By.xpath("//ul[@id='group-contact-list']/li")).click();
        driver.findElement(By.linkText("Save group")).click();
        driver.findElement(By.linkText("ACCOUNT")).click();
        driver.findElement(By.name("action")).click();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}

