import org.testng.annotations.*;

public class MyTest extends TestBase {

    @Test
    public void testMyTest() throws Exception {
        goToAddressBook();
        addNewContact(new Contact("Sam", "Smith", "улица Баумана", "Казань", "Респ. Татарстан", "(789) 347-9322", "Apr", "7th"));
        createNewGroup();
        addContactToGroup();
        logout();
    }
}

