import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestBase {
    protected WebDriver driver;
    protected String baseUrl;
    protected boolean acceptNextAlert = true;
    protected StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "https://www.postable.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        goToHomePage();
        Account user = new Account("tes.testing2017@yandex.ru", "testing");
        login(user);
    }

    protected void goToHomePage() {
        driver.get(baseUrl + "address-book/");
    }

    protected void login(Account user) {
        goToLoginPage();
        fillTheField("email", user.login);
        fillTheField("password", user.password);
        click();
    }

    protected void goToLoginPage() {
        goToLink("LOGIN");
    }

    protected void goToAddressBook() {
        goToLink("ADDRESS BOOK");
    }

    protected void logout() {
        goToAccountPage();
        click();
    }

    protected void goToAccountPage() {
        goToLink("ACCOUNT");
    }

    protected void addContactToGroup() {
        select("//ul[@id='group-contact-list']/li");
        goToLink("Save group");
    }

    protected void createNewGroup() {
        goToLink("new group");
        fillTheField("group_name", "Work");
        goToLink("Create group");
    }

    protected void addNewContact(Contact contact) {
        goToLink("ADD NEW");
        fillTheField("contact[firstname]", contact.firstname);
        fillTheField("contact[lastname]", contact.lastname);
        fillTheField("contact[address]", contact.address);
        fillTheField("contact[city]", contact.city);
        fillTheField("contact[state]", contact.state);
        fillTheField("contact[phone]", contact.phone);
        fillTheSelect("contact[birth_month]", contact.birth_month);
        fillTheSelect("contact[birth_day]", contact.birth_day);
        button("button.button-panel.button-save-contact");
    }


    protected void fillTheField(String field, String value) {
        WebElement element = driver.findElement(By.name(field));
        driver.findElement(By.name(field)).clear();
        driver.findElement(By.name(field)).sendKeys(value);
    }

    protected void fillTheSelect(String field, String value) {
        new Select(driver.findElement(By.name(field))).selectByVisibleText(value);
    }

    protected void goToLink(String link) {
        WebElement element = driver.findElement(By.linkText(link));
        driver.findElement(By.linkText(link)).click();
    }

    protected void click() {
        driver.findElement(By.name("action")).click();
    }

    protected void button(String button) {
        driver.findElement(By.cssSelector(button)).click();
    }

    protected void select(String element) {
        driver.findElement(By.xpath(element)).click();
    }

    @AfterClass(alwaysRun = true)
    protected void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    protected boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    protected String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
