import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AppManager {
    private String baseUrl;
    protected WebDriver driver;
    protected LoginHelper loginHelper;
    protected NavigationHelper navigationHelper;
    protected CardsHelper cardsHelper;
    protected ContactHelper contactHelper;
    protected GroupHelper groupHelper;
    protected StringBuffer verificationErrors = new StringBuffer();
    protected Settings settings;

    private static ThreadLocal <AppManager> app = new ThreadLocal <AppManager>();

    public AppManager() {
        driver = new FirefoxDriver();
        settings = new Settings();
        baseUrl = settings.getBaseUrl();
        loginHelper = new LoginHelper(this);
        navigationHelper = new NavigationHelper(this, baseUrl);
        cardsHelper = new CardsHelper(this);
        contactHelper = new ContactHelper(this);
        groupHelper = new GroupHelper(this);
    }


    @AfterClass(alwaysRun = true)
    protected void stop() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public static AppManager getInstance (){
        if (app == null) {
            AppManager newInstance = new AppManager();
            newInstance.navigationHelper.goToHomePage();
            app.set(newInstance);
        }
        return app.get();
    }
}
