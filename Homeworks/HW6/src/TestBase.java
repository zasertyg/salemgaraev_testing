import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;

public class TestBase {

    protected AppManager app;

    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        app = new AppManager();
        app.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        app.navigationHelper.goToHomePage();
    }

    public static String generateRandomString(int max) {
        StringBuilder result = new StringBuilder();
        String symbols = "abcdefghijknlmopstrqvzxyw";
        Random random = new Random();
        for (int i=0; i< max; i++){
            result.append(symbols.charAt(random.nextInt(symbols.length())));
        }
        return  result.toString();
    }
}
