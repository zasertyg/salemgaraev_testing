import org.testng.annotations.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;

@Test
public class MyTest extends AuthBase {

    @DataProvider
    public Object[][] cardsFromXml() {
        ArrayList<String> result = new ArrayList<>();
        try {
            File xmlFile = new File("src/cards.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("cards");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element = (Element) node;
                result.add(new String(element.getElementsByTagName("value").item(0).getTextContent()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Object[][] obj1 = (Object[][]) result.toArray();
        return obj1;
    }

    @DataProvider
    public Object[][] contactNameFromXml() {
        ArrayList<Contact> result = new ArrayList<>();
        try {
            File xmlFile = new File("src/names.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("names");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element = (Element) node;
                result.add(new Contact(element.getElementsByTagName("firstname").item(0).getTextContent(),
                        element.getElementsByTagName("lastname").item(0).getTextContent()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Object[][] obj1 = (Object[][]) result.toArray();
        return obj1;
    }

    @DataProvider
    public Object[][] groupsFromXml() {
        ArrayList<String> result = new ArrayList<>();
        try {
            File xmlFile = new File("src/groups.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("groups");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element = (Element) node;
                result.add(new String(element.getElementsByTagName("value").item(0).getTextContent()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Object[][] obj1 = (Object[][]) result.toArray();
        return obj1;
    }

    @Test(dataProvider = "cardsFromXml")
    public void search(String value) throws Exception {
        app.cardsHelper.search(value);
    }

    @Test(dataProvider = "contactNameFromXml")
    public void addNewContact(Contact contact) throws Exception {
        app.navigationHelper.goToAddressBook();
        app.contactHelper.addNewContact(new Contact(contact.firstname, contact.lastname,
                "улица Баумана", "Казань", "Респ. Татарстан", "(789) 347-9322", "Apr", "7th"));
    }

    @Test(dataProvider = "groupsFromXml")
    public void createNewGroup(String group) throws Exception {
        app.navigationHelper.goToAddressBook();
        app.groupHelper.createNewGroup(group);
        app.groupHelper.addContactToGroup();
    }

    public void randomNewGroup() throws Exception {
        app.navigationHelper.goToAddressBook();
        String group = generateRandomString(5);
        app.groupHelper.createNewGroup(group);
        app.groupHelper.addContactToGroup();
    }
}

