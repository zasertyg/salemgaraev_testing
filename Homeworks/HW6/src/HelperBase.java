import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.*;

public class HelperBase {
    protected WebDriver driver;
    protected AppManager manager;
    protected boolean acceptNextAlert = true;

    public HelperBase(AppManager manager) {
        this.manager = manager;
        driver = manager.driver;
    }

    public void fillTheField(String field, String value) {
        WebElement element = driver.findElement(By.name(field));
        driver.findElement(By.name(field)).clear();
        driver.findElement(By.name(field)).sendKeys(value);
    }

    public void fillTheSelect(String field, String value) {
        new Select(driver.findElement(By.name(field))).selectByVisibleText(value);
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    public String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}
