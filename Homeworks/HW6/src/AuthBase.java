import org.testng.annotations.BeforeClass;

public class AuthBase extends TestBase {
    @BeforeClass(alwaysRun = true)
    protected void setUpAuth() throws Exception {
        app.loginHelper.login(new Account(app.settings.getLogin(), app.settings.getPassword()));
    }
}
