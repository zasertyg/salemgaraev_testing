
public class Contact {
    String prefix;
    String firstname;
    String lastname;
    String address;
    String apt;
    String city;
    String state;
    String zip;
    String country;
    String phone;
    String email;
    String birth_month;
    String birth_day;
    String spouse_prefix;
    String spouse_firstname;
    String spouse_lastname;
    String notes;

    public Contact (String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Contact(String firstname, String lastname, String address, String city, String state, String phone, String birth_month, String birth_day) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.city = city;
        this.state = state;
        this.phone = phone;
        this.birth_month = birth_month;
        this.birth_day = birth_day;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getApt() {
        return apt;
    }

    public void setApt(String apt) {
        this.apt = apt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirth_month() {
        return birth_month;
    }

    public void setBirth_month(String birth_month) {
        this.birth_month = birth_month;
    }

    public String getBirth_day() {
        return birth_day;
    }

    public void setBirth_day(String birth_day) {
        this.birth_day = birth_day;
    }

    public String getSpouse_prefix() {
        return spouse_prefix;
    }

    public void setSpouse_prefix(String spouse_prefix) {
        this.spouse_prefix = spouse_prefix;
    }

    public String getSpouse_firstname() {
        return spouse_firstname;
    }

    public void setSpouse_firstname(String spouse_firstname) {
        this.spouse_firstname = spouse_firstname;
    }

    public String getSpouse_lastname() {
        return spouse_lastname;
    }

    public void setSpouse_lastname(String spouse_lastname) {
        this.spouse_lastname = spouse_lastname;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
