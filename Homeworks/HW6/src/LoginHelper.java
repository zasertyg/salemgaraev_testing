import org.openqa.selenium.By;

public class LoginHelper extends HelperBase{

    public LoginHelper(AppManager manager) {
        super(manager);
    }

    public void login(Account user) {
        if (isLoggedIn()) {
            if (!isLoggedIn(user)) {
                manager.loginHelper.logout();
            }
        }

        manager.navigationHelper.goToLoginPage();
        fillTheField("email", user.login);
        fillTheField("password", user.password);
        manager.navigationHelper.click();
    }

    public void logout() {
        if (isLoggedIn()) {
            manager.navigationHelper.goToAccountPage();
            manager.navigationHelper.click();
        }
    }

    public boolean isLoggedIn(){
        return !(isElementPresent(By.linkText("LOGIN")));
    }

    public boolean isLoggedIn(Account user){
        return isLoggedIn() && (getUserName().equals(user.login));
    }

    public String getUserName() {
        manager.navigationHelper.goToAccountPage();
        String text = driver.findElements(By.className("request-email")).get(0).findElement(By.tagName("span")).getText();
        return text;
    }
}
