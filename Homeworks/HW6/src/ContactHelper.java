
public class ContactHelper extends HelperBase{

    public ContactHelper(AppManager manager) {
        super(manager);
    }

    public void addNewContact(Contact contact) {
        manager.navigationHelper.goToLink("ADD NEW");
        fillTheField("contact[firstname]", contact.firstname);
        fillTheField("contact[lastname]", contact.lastname);
        fillTheField("contact[address]", contact.address);
        fillTheField("contact[city]", contact.city);
        fillTheField("contact[state]", contact.state);
        fillTheField("contact[phone]", contact.phone);
        fillTheSelect("contact[birth_month]", contact.birth_month);
        fillTheSelect("contact[birth_day]", contact.birth_day);
        manager.navigationHelper.button("button.button-panel.button-save-contact");
    }
}
