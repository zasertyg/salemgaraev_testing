
public class CardsHelper extends HelperBase {

    public CardsHelper(AppManager manager) {
        super(manager);
    }

    public void search(String s) {
        manager.navigationHelper.goToLink("PROJECTS");
        manager.navigationHelper.button("a.cat-search-open");
        fillTheField("keyword", s);
    }
}
