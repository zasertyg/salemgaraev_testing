import org.testng.annotations.*;

public class MyTest extends TestBase {

    @Test
    public void testMyTest() throws Exception {
        app.navigationHelper.goToAddressBook();
        app.contactHelper.addNewContact(new Contact("Sam", "Smith", "улица Баумана", "Казань", "Респ. Татарстан", "(789) 347-9322", "Apr", "7th"));
        String group = generateRandomString(5);
        app.groupHelper.createNewGroup(group);
        app.groupHelper.addContactToGroup();
        app.loginHelper.logout();
    }
}

