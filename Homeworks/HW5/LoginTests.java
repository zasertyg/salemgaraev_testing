import org.junit.Assert;
import org.testng.annotations.Test;

public class LoginTests extends TestBase {
    @Test
    public void loginWithValidCreditionals() throws Exception {
        Account user = new Account(app.settings.getLogin(), app.settings.getPassword());
        app.loginHelper.logout();
        app.loginHelper.login(user);
        Assert.assertEquals(app.loginHelper.isLoggedIn(user), true);
    }
}
