
public class GroupHelper extends HelperBase {

    public GroupHelper(AppManager manager) {
        super(manager);
    }

    public void addContactToGroup() {
        manager.navigationHelper.select("//ul[@id='group-contact-list']/li");
        manager.navigationHelper.goToLink("Save group");
    }

    public void createNewGroup() {
        manager.navigationHelper.goToLink("new group");
        fillTheField("group_name", "Work");
        manager.navigationHelper.goToLink("Create group");
    }
}
