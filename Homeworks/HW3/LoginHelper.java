
public class LoginHelper extends HelperBase{

    public LoginHelper(AppManager manager) {
        super(manager);
    }

    public void login(Account user) {
        manager.navigationHelper.goToLoginPage();
        fillTheField("email", user.login);
        fillTheField("password", user.password);
        manager.navigationHelper.click();
    }

    public void logout() {
        manager.navigationHelper.goToAccountPage();
        manager.navigationHelper.click();
    }
}
