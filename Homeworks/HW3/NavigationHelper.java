import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class NavigationHelper extends HelperBase {

    private String baseUrl;

    public NavigationHelper(AppManager manager, String baseUrl) {
        super(manager);
        this.baseUrl = baseUrl;
    }

    public void goToHomePage() {
        driver.get(baseUrl + "address-book/");
    }

    public void goToLoginPage() {
        goToLink("LOGIN");
    }

    public void goToAddressBook() {
        goToLink("ADDRESS BOOK");
    }


    public void goToAccountPage() {
        goToLink("ACCOUNT");
    }

    public void goToLink(String link) {
        WebElement element = driver.findElement(By.linkText(link));
        driver.findElement(By.linkText(link)).click();
    }


    public void click() {
        driver.findElement(By.name("action")).click();
    }

    public void button(String button) {
        driver.findElement(By.cssSelector(button)).click();
    }

    public void select(String element) {
        driver.findElement(By.xpath(element)).click();
    }
}
