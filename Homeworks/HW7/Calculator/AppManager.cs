﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoItX3Lib;

namespace AutoTests
{
    public class AppManager
    {

        AutoItX3 aux;
        OperationHelper operationHelper;
        public static string WINTITLE = "calc";

        public AppManager()
        {
            aux = new AutoItX3();
            aux.Run(@"C:\Windows\System32\calc.exe");
            operationHelper = new OperationHelper(this);
            aux.WinWait(WINTITLE);
            aux.WinActivate(WINTITLE);
            aux.WinWaitActive(WINTITLE);
        }

        public void Stop()
        {
            aux.WinClose(WINTITLE);
        }

        public AutoItX3 Aux
        {
            get
            {
                return aux;
            }
        }

        public OperationHelper Operations
        {
            get
            {
                return operationHelper;
            }
        }
    }

}
