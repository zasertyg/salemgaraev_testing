﻿
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace AutoTests
{
    [TestFixture]
    public class FunctionOperationTests : TestBase
    {
        [Test]
        public void AdditionTest()
        {
            OperationDataInt items = new OperationDataInt()
            {
                First = 4,
                Sign = "SQR"
            }
            ;

            manager.Operations.EnterNumber(items.First);
            manager.Operations.EnterSign(items.Sign);
            manager.Operations.Result();

            string result = manager.Operations.GetResult();
            string shouldBe = manager.Operations.CalculateFunction(items);

            NUnit.Framework.Assert.AreEqual(result, shouldBe);
        }
    }
}
